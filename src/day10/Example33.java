package day10;

import java.util.Scanner;

class OuterClassDemo{
	
	// non-staic inner class
	private class InnerClassDemo{			// nested class or inner class used for security purpose
		
		void getDetailsInnerClassDemo() {
			System.out.println("Enter your password : ");
			Scanner refScanner = new Scanner(System.in);
			String password = refScanner.next();
			System.out.println(password);
		}
		
	} // end of InnerClassDemo class
	
	 //InnerClassDemo refInnerClassDemo = new InnerClassDemo();				Approach 1

} // end of OuterClassDemo class


public class Example33 {

	public static void main(String[] args) {
		
		OuterClassDemo refOuterClassDemo = new OuterClassDemo();
		//refOuterClassDemo.refInnerClassDemo.getDetailsInnerClassDemo();		Approach 1

	//	OuterClassDemo.InnerClassDemo refInnerClassDemo = refOuterClassDemo.new InnerClassDemo();		// Approach 2
	//	refInnerClassDemo.getDetailsInnerClassDemo();
	}

}
