package test;

public class ExceptionTest01 {

	static Integer number;
	
	public static void main(String[] args) {
		
		try {
			float value = 10.50f;
			System.out.println(value/number.floatValue());
		}
		catch(ArithmeticException | NullPointerException refException) {
			System.out.println(refException.getClass().getSimpleName());
		}

	} // end of main()

} 

