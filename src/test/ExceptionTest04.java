package test;

public class ExceptionTest04{
	void method1() {
		System.out.println("Inside method1");
		try {
			method2();
		} // end of try
		catch(ArithmeticException refArithmeticException) {
			System.out.println("Hello-B");
		}
		finally {
			System.out.println("Hello-C");
		}
		System.out.println("Hello-D");
	} // end of method1
	void method2() {
		System.out.println("Inside method2");
		Object refObject = null;
		refObject.toString();
		System.out.println("Hello-F");
	} // end of method2
	
	public static void main(String[] args) {
		new ExceptionTest04().method1();
	}
}


























/*
 * public class ExceptionTest04 {
 * 
 * public static void main(String[] args) {
 * 
 * try { long size = 10; int [] number = new int[size];
 * 
 * number[1] = 10;
 * 
 * System.out.println(number[5] + number[6]); }
 * 
 * catch(Exception refException) { refException.printStackTrace(); }
 * 
 * }
 * 
 * }
 */
