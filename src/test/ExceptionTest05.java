package test;

class BusinessIntelligence{
	public String name;
	public void getData() {
		System.out.println("Hello-A");
		try {
			System.out.println("Hello-B");
			int number = Integer.parseInt(name);
			System.out.println("Hello-C");
		} // end of try
		catch(NumberFormatException exception) {
			System.out.println("Hello-D");
		} // end if catch
	} // end of getData()
	
} // end of BusinessIntelligence

public class ExceptionTest05 {
	public static void main(String... args) {
		BusinessIntelligence refBusinessIntelligence = new BusinessIntelligence();
		refBusinessIntelligence.name = "TensorFlow";
		refBusinessIntelligence.getData();
	}
}
