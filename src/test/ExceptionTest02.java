package test;

public class ExceptionTest02 {

	public static void main(String[] args) {
		
		try {
			print("10.0");
		}
		catch(Exception ref){
			System.out.print("hello-1");
		}

	} // end of main()
	
	public static void print(String data) {
		try {
			System.out.println(Integer.parseInt(data));
		}
		catch (NumberFormatException e) {
			System.out.print("Hello-2");
			throw new RuntimeException();
		}
		finally {
			System.out.println("Hello-3");
		}
	}

}
