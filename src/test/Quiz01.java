package test;

// Complexity : Hard

public class Quiz01 {

	static Integer number;
	
	public static void main(String[] args) {

		String refString;
		try {
			refString = number.toString();
		}
		finally {
			try {
				var number1 = Integer.parseInt(args[0]);
			}
			catch(NumberFormatException ref) {
				System.out.println("NumberFormatException Handled");
			}
			finally {
				System.out.println("closing files");
			}
			System.out.println("Hello");
		}
	
	}
}
