package test;

public class TestDemo03 {

	TestDemo03(String data){
		name = data;
	}
	
	public static void main(String[] args) {
		TestDemo03 refTestDemo1 = new TestDemo03("data1");
		TestDemo03 refTestDemo2 = new TestDemo03("data2");

		refTestDemo1 = refTestDemo2;
		refTestDemo2 = null;
		refTestDemo1 = null;
	}
	
	private String name;

}

