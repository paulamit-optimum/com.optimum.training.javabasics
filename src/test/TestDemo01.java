package test;

public class TestDemo01 {
	
	private static int numberOfInvoices = 0;
	private static int numberOfInvoice = 0;
	
	TestDemo01(){
		numberOfInvoice++;
		numberOfInvoices = numberOfInvoice;
		getNumberOfInvoice();
	}
	
	public int getNumberOfInvoice() {
		numberOfInvoices = numberOfInvoices + 1;
		incrementInvoices();
		return numberOfInvoices;
	}
	
	public int incrementInvoices() {
		return numberOfInvoices++;
	}

	public static void main(String[] args) {
		
		TestDemo01 refTestDemo = new TestDemo01();
		System.out.println(refTestDemo.incrementInvoices());
 
	} // end of main()
	
} // end of TestDemo01

