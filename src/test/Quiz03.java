package test;

class Employee {
	public Employee() {
		System.out.println("Hello Employee");
	}
	
	static {
		System.out.println("Hello Visitor");
	}
}

class Admin extends Employee{
	public Admin() {
		System.out.println("Hello Admin");
	}
	
	{
		System.out.println("Hello User");
	}
}

public class Quiz03 {

	public static void main(String[] args) {
		
		Employee refEmployee = new Admin();

	}

}
