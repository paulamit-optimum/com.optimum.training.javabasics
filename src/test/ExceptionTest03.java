package test;

public class ExceptionTest03 {

	public static void main(String[] args) {
		
		try {
			
		} 
		  catch(ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException refException) {
		
			if (refException instanceof ArrayIndexOutOfBoundsException) {
		//		refException = new ArrayIndexOutOfBoundsException("Out of Bounds");
			} else if(refException instanceof NullPointerException){
		//		refException = new NullPointerException("Null Value");
			}
			else {
		//		refException = new ArithmeticException("Arithmetic");
			}
			
			System.out.println(refException.getMessage());
		}
		

	}

}

//catch(Exception refException) {	// no error