package test;

import java.util.Arrays;
import java.util.Comparator;

class Compare implements Comparator<String>{

	@Override
	public int compare(String s1, String s2) {
		return s2.length() - s1.length();
	}
	
}

public class TestDemo08 {

	public static void main(String[] args) {
		
		String[] names = {"Mary","Jane", "Elizabeth","Jo"};
		Arrays.sort(names,new Compare());
		for(String name:names) {
			System.out.println(name);
		}
		
		String[] names1 = {"Mary","Jane", "Ann","Tom"};
		Arrays.sort(names1);
		int x = Arrays.binarySearch(names1, "Ann");
		System.out.println(x);
	}

}
