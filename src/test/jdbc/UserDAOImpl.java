package test.jdbc;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import utility.DBUtility;

public class UserDAOImpl{

	Connection refConnection = null;
	PreparedStatement refPreparedStatement =null;
	ResultSet refResultSet = null;
	
	CallableStatement refCallableStatement = null; // for stored procedure
	
	public void getUserRecord() {
		// var sqlSuery = "call getUsers";

		var sqlQuery = "select * from user";
					
		try {
			refConnection = DBUtility.getConnection();
			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			
			var refResultSet = refPreparedStatement.executeQuery();
			refResultSet.next();
				System.out.println(refResultSet.getInt(1));
			
			
				//refResultSet = refCallableStatement.executeQuery();
				//refCallableStatement = refConnection.prepareCall(sqlSuery);
		}
		catch(Exception refException) {
		
			System.out.println("Exception Handled while retrieving record.");
			
		}
		finally {
			System.out.println("\nClosing connection..");
		}
		
	}
/*
	public void insertRecord() {
				
					
		// Using PreparedStatement interface
//		var sqlQuery = "insert into user(user_id,user_password) values(?,?)";

		// Using CallableStatement interface
		var sqlQuery = "{call insertRecord(?,?)}";
		
		try {
			refConnection = DBUtility.getConnection();
	
			// Using PreparedStatement interface
			
			// refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			// refPreparedStatement.setInt(1, refUser.getUserID());
			// refPreparedStatement.setString(2, refUser.getUserPassword());
	
			// Approach 1 - execute
			// refPreparedStatement.execute();  works fine
			
			// Approach 2 - executeUpdate
			// var record = refPreparedStatement.executeUpdate();
			// if(record>0) System.out.println("new record has been successfully inserted");

			
			// Using CallableStatement interface
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);
			refCallableStatement.setInt(1, refUser.getUserID());
			refCallableStatement.setString(2, refUser.getUserPassword());

			refCallableStatement.execute();    
			// addBatch();
			// executeBatch();
			System.out.println("new record has been successfully inserted");
			
			
		} catch (SQLException e) {
			System.out.println("Exception Handled while insert record.");
		}
		
		finally {
			try {
				refConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			finally {
				System.out.println("Closing Connection..");
			}
		}
		
	} // insertRecord

	
	public void deleteRecord() {
		
		refConnection = DBUtility.getConnection();
		
		try {
			
			var sqlQuery = "call deleteRecord(?)";
			
			//var sqlQuery = "delete from user where user_id=?";
			//refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
			//refPreparedStatement.setInt(1,refUser.getUserID()); 
			
			// executeUpdate() or execute()
			//refPreparedStatement.executeUpdate();
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);
		//	refCallableStatement.setInt(1, refUser.getUserID());
			
			 refCallableStatement.executeUpdate();
			
			System.out.println("Record deleted Successfully..");
		}
		
		catch(Exception refException) {
			System.out.println("Exception Handled while deleteing the record..");
		}
		
		finally {
			System.out.println("Closing connection..");
		}
		
	}

	
	public void updateRecord() {
		
		refConnection = DBUtility.getConnection();
		
		try {
			
//			var sqlQuery = ("update user set user_password=? where user_id=?");
		
//			refPreparedStatement = refConnection.prepareStatement(sqlQuery);
			
//			refPreparedStatement.setInt(2, refUser.getUserID());
//			refPreparedStatement.setString(1,refUser.getUserPassword());
			
//			refPreparedStatement.executeUpdate();
	
			var sqlQuery = "call updateRecord(?,?)";
			
			refCallableStatement = refConnection.prepareCall(sqlQuery);	
			
			refCallableStatement.setInt(1, refUser.getUserID());
			refCallableStatement.setString(2, refUser.getUserPassword());
			
			refCallableStatement.executeUpdate(); 
			
			
			System.out.println("Successfully Updated..");
		}
		catch(Exception refException) {
			System.out.println("Exception Handled while updating record.");
		}
		finally {
			System.out.println("Closing Connection..");
		}
	}

	
	public void getUserByID() {
		
		refConnection = DBUtility.getConnection();
		
		try {
		var sqlQuery = "call getRecordByIDnew(?,?)";	
	//	var sqlQuery = "call fetchRecordByID(?,?)"; 	// NULL
		
		refCallableStatement = refConnection.prepareCall(sqlQuery);
		refCallableStatement.setInt(1,refUser.getUserID());
	//	refCallableStatement.setString(2, refUser.getUserPassword());
	
	//	refCallableStatement.registerOutParameter(2,java.sql.Types.VARCHAR);
		
		refCallableStatement.executeQuery(); // executeUpdate() and executeQuery() also works fine
		System.out.println(refCallableStatement.getString(2));
		
		}
		catch(Exception refException) {
			System.out.println("Exception Handled while getting record by ID.");
		}
		
		finally {
			System.out.println("Closing Connection..");
		}
	}
*/
}
