package test;

class Printer{
	private static void print1(int number) {
		System.out.println(number*2);
	}
	
	static void print2(int number) {
		System.out.println(number);
	}
}

public class Quiz05 {

	public static void main(String[] args) {
		try {
			var number = 0.00/0.00;
			Printer refPrinter = new Printer();
			//Printer.refPrinter.print2(number);
		}
		catch(Exception ref) {
			System.out.println("Exception Handled");
		}
		finally {
			System.out.println("Closing files");
		}
		
	}
}

// refPrinter.print2(10);
