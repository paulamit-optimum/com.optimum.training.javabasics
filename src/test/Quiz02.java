package test;

public class Quiz02 {

	public static void main(String[] args) {
		
		var number = 15;
		while(number>0) {
			do {
				number = 2;
				System.out.println(number);
			}
			while(number>5);
			number = number--;
			System.out.println(number);
		}
		
	}

}
