package test;

class User extends Exception{
	
	User(String value){
		System.out.println(value);
	}
	
	static void getNumber(int number) {
	
		try {
			if(number>100) 
				throw new IllegalArgumentException("Value must be less than 100");
			System.out.println(number);
		}
		catch(IllegalArgumentException ref) {
			System.out.println("Out of range");
		}
		
	} // end of getNumber
} // end of User

public class ExceptionTest06 {
	public static void main(String[] args) {
		User.getNumber(199);
	}
}

