package day3;

// concept of local and global variable

class User{
	
	int userID =1000; 				// global varibale
	String userPassword="user123";  // global variable
	
	void getDetails(int userID, String password) { // local variable ==> 50 and newuser123
		
		// local variable needs to pass the value to global variable
		
		this.userID = userID;   
		userPassword = password;
		 
	
	} // end of showDetails()
	
	
	void showDetails() {
		
		System.out.println(userID + " "+userPassword); // we are calling global variable 
	
	} // end of showDetails()
	
} // end of User


public class Example05 {

	public static void main(String[] args) {
		
		User refUser = new User();
		refUser.getDetails(50,"newuser123");  // line 31 is calling line 10
		refUser.showDetails();

	}

}
