package day3;

class UserInformation {
	void getUserDetails(Object data) {  // get the parameters
		System.out.println(data); // print the value
	} // end of getUserDetails
} // end of UserInformation

class Person{
	String personName = "Gosling";
	@Override
	public String toString() {
		return personName;
	}
} // end of Person

public class Example09 {
	public static void main(String[] args) {
		int userID = 100;
		String userNname = "James";
		float userCode = 1234.543f;

		// call getUserDetails() and pass the values
		UserInformation refUserInformation = new UserInformation();
		refUserInformation.getUserDetails(userID);
		refUserInformation.getUserDetails(userNname);
		refUserInformation.getUserDetails(userCode);
		
		Person refPerson = new Person();
		refUserInformation.getUserDetails(refPerson); 
	} // end of main
} //end of Example09


