package day9;

import java.util.Scanner;

// do while loop


public class Example30 {

	public static void main(String[] args) {
	
		Object refObject;
		
		String userChoise=null;
		do {
		Scanner scannerRef = new Scanner(System.in);
		System.out.println("Enter your name : ");
		
		String userName = scannerRef.next();
		
		System.out.println("Wish to continue(yes/no) : ");
		
		userChoise = scannerRef.next();
		
		} while(!userChoise.equals("no"));
		
		System.out.println("Thank You!!");
	}

}
