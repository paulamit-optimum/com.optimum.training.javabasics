package sprint3quiz1;

import java.util.Arrays;

public class Problem03 {

	public static void main(String[] args) {
		
		String [] refString = {"A", "B", "C", "D"};

		Arrays.sort(refString);
		
			try {
				for(var temp : refString) {
					System.out.println(temp);
				} // end of for
			} // end of try
			
			catch(Exception exception){
				System.out.println("exception handled");
			}
			
			finally {
				System.out.println("closing files");
			}
			
	} // end of main

} // end of Problem01
