package sprint3quiz1;

enum Status{
	
	FAST(2),
	FASTER(1),
	SLOW(1);
	
	private final int speed;
	
	Status(int speedCode){
		speed = speedCode;
	}
}

public class Problem008 {

	public static void main(String[] args) {
		
		System.out.println(Status.FASTER == Status.FAST);

	}

}
