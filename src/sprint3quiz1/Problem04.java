package sprint3quiz1;

import static java.lang.Math.*;

class HandleException{
	static void handleException() {
		System.out.println("calling next process");
		doNextProcess();
	}
	static void doNextProcess() {
		System.out.println("Doing next process");
	}
} // end of HandleException

public class Problem04 {
	public static void main(String[] args) {
		
		try {
			var value = PI;
			System.out.println(value/0);
		}
		catch(ArithmeticException refException) {
			System.out.println("Exception Handled");
		}
		finally {
			HandleException.handleException();
			System.out.println("Closing files");
		}
	}
}
