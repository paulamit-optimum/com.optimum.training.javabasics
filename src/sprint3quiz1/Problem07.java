package sprint3quiz1;

public class Problem07 {

	static String number1;
	static long number2;
	
	Problem07(){
		number1 = "blue";
		number2 = 100;
	}
	
	public static void main(String[] args) {
		
		var refProblem1 = new Problem07();
		var refProblem2 = new Problem07();

		refProblem1.number1 = "black";
		refProblem2.number2 = 1000;
		
		System.out.println(refProblem1.number1);
		System.out.println(refProblem2.number2);
	}

}
