package sprint3quiz1;

public class Problem02 {

	public static void main(String[] args) {
		
		int number1 = 7, number2 = 2;
		
		boolean data1 = number1>1 & (number2<9 || number1<2);
		
		boolean data2 = (number2>2) && (number1++>1);
		
		boolean data3 = 7<= --number1;
		
		System.out.println(data1+ " "+data2+ " "+data3);

	}

}
