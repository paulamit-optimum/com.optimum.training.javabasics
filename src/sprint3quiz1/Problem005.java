package sprint3quiz1;

public class Problem005 {

	public static void main(String[] args) {
		
		var number1 = 1;
		var number2 = 2;
		
		var number3 = (++number1*number2--)*--number2;
		
		System.out.println(number3);
	}

}
