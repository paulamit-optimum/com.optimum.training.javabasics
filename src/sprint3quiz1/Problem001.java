package sprint3quiz1;

public class Problem001 {

	public static void main(String[] args) {
		
		int array[][] = {{1,2,3},{4,5,6},{7,8,9}};

		outer:for(int x=0, k=0 ; x<3 ; x++) {
			k=0;
			inner:while(true) {
				System.out.println(array[x][k++]);
				if (k>3) break inner;
			}
		}
	}

}


// if (k==3) break outer;		1 2 3

// if (k==3) break inner;		1 2 3 4 5 6 7 8 9

// if (k>3) break inner;		1 2 3 followed by ArrayIndexOutOfBoundsException

// break;						1 4 7

// 