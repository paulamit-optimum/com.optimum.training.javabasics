package sprint3quiz1;

import java.util.function.Predicate;

public class Problem06 {

	int number;
	
	private static void check(Problem06 refProblem, Predicate<Problem06> refPredicate) {
		String result = refPredicate.test(refProblem) ? "match" : "not match";
		System.out.println(result);
	}
	
	public static void main(String[] args) {
		
	//	Problem06 ref = new Problem06();
	//	ref.number = 1;
	//	check(ref,ref->{ref.number<5});

	}
}
