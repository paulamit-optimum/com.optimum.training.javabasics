package day12;

// Rule of Varargs
// there can be only one variable argument(varargs) in the method
// and varargs must be the last argument

public class VarArgsDemo1 {
	
	public static void test1(int data1, String... data2) {
		for (String temp : data2) {
			System.out.println(temp);
		}
	}
	
	// error
	
	/*
	 * public static void test2(int[]data , int... data) {
	 * 
	 * }
	 */	 
	
	public static void test3(int data1[], int data2[]) {
		// get the arrays
	}
	
	public static void main(String[] args) {
		
		test1(10,"value1","value2","valu-n");
		
		String name[] = {"Kenny","Heah"};
		
		test1(50,name);

	}

}
