package quiz10;

interface Customer1{
	default void want() {
		System.out.println("1");
	}
	static void login() {}
	void logout();
	private int viewDetails() {
		return 50;
	}
} // end of Customer1

interface Customer2{
	default void want() {
		System.out.println("2");
	}
	static void login() {}
	void logout();
	private int viewDetails() {
		return 20;
	}
} // end of Customer2

public class Quiz10Test6 implements Customer1,Customer2{

	public static void main(String[] args) {
		Quiz10Test6 ref = new Quiz10Test6();
		ref.want();
		ref.logout();
	//	ref.viewDetails();
	}

	@Override
	public void want() {
		System.out.println("3");
	}

	@Override
	public void logout() {
		System.out.println("logging out");
		
	}

}
