package quiz10;

abstract class Programming{
	protected int count;
	public abstract int getProgramming();
}


public class Quiz10Test5 extends Programming{

	private int number;
	
	Quiz10Test5(int number){
		this.number=number;
	}
	
	public static void main(String[] args) {
		Programming refProgramming = new Quiz10Test5(3);
		System.out.println(refProgramming.getProgramming());
	}

	@Override
	public int getProgramming() {
		return this.number/count;
	}

}
