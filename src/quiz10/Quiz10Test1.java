package quiz10;

public class Quiz10Test1 {

	public static void main(String[] args) {
		
		var refStringBuilder = new StringBuilder("data1");
		var refStringBuffer = new StringBuffer("data2");
		refStringBuilder.append("data3");
		refStringBuilder.append("data4");
		refStringBuffer.append("data5");
		refStringBuffer.append("data6");
		
		System.out.println(refStringBuilder.length() + " "+refStringBuilder.capacity());
		System.out.println(refStringBuffer.length() + " "+refStringBuffer.capacity());

	}

}
