package quiz12;

import java.util.stream.DoubleStream;

public class Quiz12Test01 {

	public static void main(String[] args) {
		
		var refVar = DoubleStream.of(1.2,2.4);
		refVar.peek(System.out::println).filter(x-> x >2).count();
		
	}
}
