package quiz12;

import java.util.function.Predicate;
import java.util.stream.Stream;

public class Quiz12Test06 {

	public static void main(String[] args) {
		
		Predicate<String> refPredicate = s-> s.length()>3;
		
		var refStream = Stream.iterate("-",s->!s.isEmpty(),(s)-> s+s);
		var data1 = refStream.noneMatch(refPredicate);
		var data2 = refStream.anyMatch(refPredicate);
		
		System.out.println(data1 + " "+data2);
		

	}

}
