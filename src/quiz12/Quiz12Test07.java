package quiz12;

import java.util.LinkedList;

public class Quiz12Test07 {

	public static void main(String[] args) {
		
		var refVar = new LinkedList<String>();
		refVar.offer("hello-1");
		refVar.offer("hello-2");
		refVar.offer("hello-3");
		
		refVar.pop();
		refVar.peek();
		
		while(refVar.peek()!=null) {
			System.out.println(refVar.pop());
		}

	}

}
