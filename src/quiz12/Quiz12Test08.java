package quiz12;

import java.util.HashMap;

public class Quiz12Test08 {

	public static void main(String[] args) {
		
		var refMap = new HashMap<Integer,Integer>();
		refMap.put(1,10);
		refMap.put(2,null);
		refMap.put(3,20);

		refMap.merge(1, 3,(a,b)->a+b);
		refMap.merge(3, 3,(a,b)->a+b);
		
		System.out.println(refMap);
	}

}

// {1=13, 2=null, 3=23}