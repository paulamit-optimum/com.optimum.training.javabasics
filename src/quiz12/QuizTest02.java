package quiz12;

import java.util.function.Function;

public class QuizTest02 {

	public static void main(String[] args) {
		
		Function<Integer,Integer> refFunction1 = a-> a + 4;
		Function<Integer,Integer> refFunction2 = b-> b * 5;
		Function<Integer,Integer> refFunction3 = refFunction1.compose(refFunction2);

		System.out.println(refFunction3.apply(1));
	}

}
