package day7;

public class Department {
	
	Student refStudent;
	
	Department(Student refStudent){
		this.refStudent = refStudent;
	}

	@Override
	public String toString() {
		return refStudent.toString();
	}
}
