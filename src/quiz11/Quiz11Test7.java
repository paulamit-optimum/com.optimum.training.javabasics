package quiz11;

import java.util.ArrayList;
import java.util.List;

public class Quiz11Test7 {

	static void getDetails(List<Object> ref ) {
		for (Object object : ref) {
			System.out.println(object);
		}
	}
	
	public static void main(String[] args) {
		
		List<Object> refList = new ArrayList<>();
		refList.add("Java");
		refList.add(new Integer(20));
		getDetails(refList);
	}

}
