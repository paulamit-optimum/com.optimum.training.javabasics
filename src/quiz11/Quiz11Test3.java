package quiz11;

import java.util.ArrayList;

public class Quiz11Test3 {

	public static void main(String[] args) {
		var ref = new ArrayList<>();
		ref.add(1);
		ref.add(2);
		System.out.println(ref);
		
		ref.add(Integer.valueOf(3));
		ref.add(Integer.valueOf(5));
		System.out.println(ref);
		
		ref.remove(2);
		System.out.println(ref);

	}

}
