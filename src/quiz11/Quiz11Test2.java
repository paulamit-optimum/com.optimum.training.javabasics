package quiz11;

import java.util.ArrayList;
import java.util.Iterator;

public class Quiz11Test2 {

	public static void main(String[] args) {
		
		var ref = new ArrayList<String>();
		ref.add(0, "Hello-1");
		//ref.add(true);
		ref.add("Hello-2");
		
		Iterator refIterator = ref.listIterator();
		while (refIterator.hasNext()) {
			System.out.println(refIterator.next());			
		}
	}

}
